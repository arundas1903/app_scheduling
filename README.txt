REQUIREMENTS:

Make sure to install rabbitmq as this is used as broker for the celery.

https://www.rabbitmq.com/download.html


STEPS TO MAKE PROJECT RUNNING:

Step 1:

Clone repo:

> git clone https://arundas1903@bitbucket.org/arundas1903/app_scheduling.git

Step 2:

Go to project directory and create and activate a virtualvenv
(This can be created any where. But to make things easier we do it)

> cd app_scheduling/
> virtualenv venv
> source venv/bin/activate

Step 3:

Install all requirements

> pip install -r requirements.txt

Step 4:

Migrate

> python manage.py migrate

Step 4:

Start supervisord

> python manage.py supervisor --daemonize