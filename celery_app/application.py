# coding=utf-8
from __future__ import absolute_import, unicode_literals

from celery import Celery

# Start the Celery server
app = Celery('celery_app', backend='amqp', broker='amqp://guest:guest@localhost:5672//',
                include=['celery_app.tasks'])

if __name__ == '__main__':
    app.start()
