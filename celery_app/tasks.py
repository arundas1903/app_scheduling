import smtplib
import os
import sys

os.environ['DJANGO_SETTINGS_MODULE'] = "app_scheduling.settings"

import django

django.setup()

from gcm import *

from .application import app
from base.models import AppUser
from globals import GCM_API_KEY, GCM_CALL_ENTRY_LIMIT


@app.task
def send_notification(data):
    """
        Sample data: {u'body': u'Content is required', u'schedule_time': u'2017-06-20T14:16:40', 
        u'user_ids': [], u'all_users': True, u'title': u'Title'}
    """
    try:
        if data['all_users']:
            reg_ids = AppUser.objects.all().values_list('gcm_id')
        else:
            reg_ids = AppUser.objects.filter(id__in=user_ids).values_list('gcm_id')
        gcm = GCM(GCM_API_KEY)
        data = {'title': data['title'], 'body': data['body']}
        test = []
        # Loop is required as we cannot send the push request for millions of users in one call
        for i in range(len(reg_ids)/GCM_CALL_ENTRY_LIMIT + 1):
            try:
                send_reg_ids = reg_ids[GCM_CALL_ENTRY_LIMIT * i: GCM_CALL_ENTRY_LIMIT * (i + 1)]
                test.append(send_reg_ids)
                response = gcm.json_request(registration_ids=send_reg_ids, data=data)
            except Exception as e:
                print e
    except Exception as e:
        print e
