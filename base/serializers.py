from django.utils import timezone

from rest_framework import serializers

from base.models import AppUser


class NotificationSerializer(serializers.Serializer):

    """
        Serializer to send notification
        Note:
            "body" should be minimum  of 10 characters
            "schedule_time" format is YYYY-MM-DDThh:mm[:ss[.uuuuuu]][+HH:MM|-HH:MM|Z] (Ex: 2017-06-20T19:37:14.000Z)
            "all_users" is to check whether the notification is to be send to all users
                If all_users is true notification is to be send to all users
                Else it should be send to the users with user_ids
    """

    title = serializers.CharField(required=True, allow_blank=True, max_length=100)
    body = serializers.CharField(required=True, allow_blank=True, min_length=10)
    conditional_clause = serializers.CharField(required=False, allow_blank=True)
    schedule_time = serializers.DateTimeField(format="%Y-%m-%dT%H:%M:%S")
    all_users = serializers.BooleanField(default=True)
    user_ids = serializers.ListField(required=False)

    def validate(self, data):
        """
            Check if all_users is True, If not make sure users to which the notification is send is given
            Also check if the schedule time is future time
        """
        if data['all_users'] == False and not data.get('user_ids'):
            raise serializers.ValidationError(
                "Select users to send notification to as you have un checked all users")
        if data['schedule_time'] < timezone.now():
            raise serializers.ValidationError(
                "Select Future time to send notification")
        return data
