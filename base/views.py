# -*- coding: utf-8 -*-
from datetime import datetime

from rest_framework import generics
from rest_framework.response import Response

from base.serializers import NotificationSerializer
from celery_app.tasks import send_notification


class NotificationView(generics.GenericAPIView):
    """
        API Url: /base/notify/
        Method: POST
        Input Sample: {"title": "Title", "schedule_time": "11:59:59", 
        "body": "Content is required", "all_users": false, "user_ids": [1]}
    """
    serializer_class = NotificationSerializer

    def post(self, request, format=None):
        """
            Method for POST
        """
        serializer = self.get_serializer(data=request.data)
        if serializer.is_valid():
            schedule_time = datetime.strptime(serializer.data['schedule_time'], "%Y-%m-%dT%H:%M:%S")
            send_notification.apply_async(kwargs={'data': dict(serializer.data)}, eta=schedule_time)
            return Response({'status': 1})
        return Response({'status': -1, 'error': serializer.errors})
