from django.conf.urls import url

from base.views import NotificationView

urlpatterns = [
    url(r'^notify/', NotificationView.as_view(), name='notify'),
]
