# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models

# Create your models here.

class AppUser(models.Model):
	"""
		Sample model to store all users
	"""
	username = models.CharField(max_length=100)
	gcm_id = models.TextField()
	email = models.CharField(max_length=100)
